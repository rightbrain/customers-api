<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /**
     * @Route("customers/{id}", name="customer_show", methods="GET")
     *
     * @param Request $request
     * @return Response|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function showAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');

        $customer = $repository->find($id);

        if ($customer) {
            return new Response($customer->toJson(), 200, [
                'Content-type' => 'application/json'
            ]);
        }

        throw $this->createNotFoundException('Customer with the provided ID does not exist.');
    }

    /**
     * @Route("/customers/", name="customer_create", methods="POST")
     */
    public function createAction(Request $request)
    {
        if (!$request->headers->has('X-ApiKey')) {
            return new Response('Required API key is missing.', 400);
        }

        if ($request->headers->get('X-ApiKey') != '3e66f6b0b56047b28f81e26fac118e0d') {
            return new Response('Unauthorized access.', 401);
        }

        $data = $request->request->all();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');
        $customer = $repository->create($data);

        $response = new Response($customer->toJson(), 201, [
            'Content-type' => 'application/json'
        ]);

        return $response;
    }
}
